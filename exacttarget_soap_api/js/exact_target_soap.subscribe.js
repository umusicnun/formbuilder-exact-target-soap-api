(function ($) {

  function exactTargetSoapSubscriptionInjection() {

    console.log("exactTargetSoapSubscriptionInjection");
    if ($('.exact-target-block').length) {

      $.ajax({
        type: 'GET',
        url: Drupal.settings.basePath + Drupal.settings.pathPrefix + 'js/exact-target-soap/get-subscription-form',
        success: function(response) {

          $('.exact-target-block').html(response);
          $.each($('.exact-target-block .form-type-checkbox'), function(i, field) {
            $(field).find('input').attr('checked', true);
          });
        }

      });

    }
  }

  Drupal.behaviors.exactTargetSoapSubscription = {

    /**
     * Standard Drupal behavior attach.
     *
     * Retrieves subscription form checkboxes for allowing user to subscribe to
     * Exact Target lists. Renders the form elements inside .exact-target-block.
     */
    exactTargetSoapSubscriptionInjection: function(context) {

      console.log("attach");

      $('button#capture_signIn_traditionalSignIn_createButton').click(function() {
        console.log("clicked");
        exactTargetSoapSubscriptionInjection();
      });

      exactTargetSoapSubscriptionInjection();

    },

    /**
     * Helper to inspect the DOM for subscription checkboxes and save to object.
     *
     * Retrieves checkbox values and constructs object that then gets saved in
     * this behavior's Drupal global object.
     */
    exactTargetRetrieveInput: function() {

      var userInput = {};

      if ($('.exact-target-block').length) {

        $.each($('.exact-target-block .form-type-checkbox'), function(i, field) {

          var input = $(field).find('input');

          if (input.attr('checked')) {

            userInput[input.attr('name')] = 1;

          }

        });

      }

      Drupal.behaviors.exactTargetSoapSubscription.userInput = userInput;

    },

    /**
     * Callback for processing user input from subscription checkboxes.
     *
     * Retrieves the user input and submits POST request, which processes the
     * subscription information and returns a message.
     */
    exactTargetSubscriptionCallback: function(user) {

      Drupal.behaviors.exactTargetSoapSubscription.exactTargetRetrieveInput();
      console.log('Called ExactTargetSubscriptionCallback');
      console.log(user);
      if ($.isEmptyObject(user) || $.isEmptyObject(Drupal.behaviors.exactTargetSoapSubscription.userInput)) {
        return;
      }

      // Add user to userInput.
      Drupal.behaviors.exactTargetSoapSubscription.userInput.user = user;

      console.log('url: ' + Drupal.settings.basePath + ' ' + Drupal.settings.pathPrefix + 'js/exact-target-soap/post-subscription');
      console.log('Drupal.behaviors.exactTargetSoapSubscription.userInput');
      console.log(Drupal.behaviors.exactTargetSoapSubscription.userInput);
      $.ajax({
        type: 'POST',
        data: Drupal.behaviors.exactTargetSoapSubscription.userInput,
        url: Drupal.settings.basePath + Drupal.settings.pathPrefix + 'js/exact-target-soap/post-subscription',
        success: function(response) {

          $('.exact-target-block').html(response);

        }

      });

    }

  };

}(jQuery));
