<?php
/**
 * @file
 * ExactTarget SOAP API for Drupal.
 */

// Module constants.
define('EXACT_TARGET_SOAP_CLEANUP_NOTHING', 0);
define('EXACT_TARGET_SOAP_CLEANUP_MAIL', 1);
define('EXACT_TARGET_SOAP_CLEANUP_CRON', 2);

// These two are the values that variable exact_target_soap_synchronous_api has
// to put the API communication into synchronous or asynchronous communication.
define('EXACT_TARGET_SOAP_ASYNCHRONOUS', 0);
define('EXACT_TARGET_SOAP_SYNCHRONOUS', 1);


/**
 * Implements hook_permission().
 */
function exact_target_soap_permission() {
  return array(
    'administer exact target soap' => array(
      'title' => t('Administer Exact Target settings'),
      'description' => t('Allows access to the Exact Target settings page.'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function exact_target_soap_menu() {
  $items = array();

  $items['js/exact-target-soap/get-subscription-form'] = array(
    'page callback' => 'exact_target_soap_subscribe_ui_for_embed',
    'delivery callback' => '_exact_target_soap_ahah_deliver',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
    'file' => 'exact_target_soap.subscribe.inc',
  );
  $items['js/exact-target-soap/post-subscription'] = array(
    'page callback' => 'exact_target_soap_subscribe_ui_for_embed_submit',
    'delivery callback' => '_exact_target_soap_ahah_deliver',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
    'file' => 'exact_target_soap.subscribe.inc',
  );
  $items['admin/config/services/exact_target_soap'] = array(
    'title' => 'ExactTarget SOAP API',
    'description' => 'Settings for the ExactTarget SOAP API.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('exact_target_soap_settings'),
    'access arguments' => array('administer exact target soap'),
    'file' => 'exact_target_soap.admin.inc',
  );
  $items['admin/config/services/exact_target_soap/settings'] = array(
    'title' => 'Settings',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 0,
  );
  $items['admin/config/services/exact_target_soap/status'] = array(
    'title' => 'Status',
    'page callback' => 'exact_target_soap_status_page',
    'access arguments' => array('administer exact target soap'),
    'file' => 'exact_target_soap.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
  );
  $items['admin/config/services/exact_target_soap/opt-in'] = array(
    'title' => 'Opt-In Lists',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('exact_target_soap_opt_in'),
    'access arguments' => array('administer exact target soap'),
    'file' => 'exact_target_soap.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 2,
  );
  $items['admin/config/services/exact_target_soap/queue'] = array(
    'title' => 'Queue',
    'page callback' => 'exact_target_soap_queue',
    'access arguments' => array('administer exact target soap'),
    'file' => 'exact_target_soap.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 3,
  );
  $items['admin/config/services/exact_target_soap/queue/%'] = array(
    'title' => 'ExactTarget SOAP request',
    'page callback' => 'exact_target_soap_queue_request_view',
    'page arguments' => array(5),
    'access callback' => 'user_access',
    'access arguments' => array('administer exact target soap'),
    'file' => 'exact_target_soap.admin.inc',
    'type' => MENU_CALLBACK,
  );
  $items['admin/config/services/exact_target_soap/queue/%/delete'] = array(
    'title' => 'ExactTarget SOAP request',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('exact_target_soap_queue_request_delete', 5),
    'access callback' => 'user_access',
    'access arguments' => array('administer exact target soap'),
    'file' => 'exact_target_soap.admin.inc',
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_cron().
 */
function exact_target_soap_cron() {
  // If the system should try to clean up after itself
  // try to clear the batch queue on cron runs.
  if (variable_get('exact_target_soap_cleanup', EXACT_TARGET_SOAP_CLEANUP_NOTHING) == EXACT_TARGET_SOAP_CLEANUP_CRON) {
    exact_target_soap_clear_batch();
  }
}

/**
 * Implements hook_mail().
 */
function exact_target_soap_mail($key, &$message, $params) {
  switch ($key) {
    case 'cleanup':
      $message['subject'] = t('The ExactTarget API is in batch mode');
      $message['body'][] = t(
        'One or more ExactTarget API requests has failed unexpectedly and forced the site into batch mode. ' .
          'You should clear the batch using the drush command: exact-target-soap-clear-batch or by manually ' .
          'processing the batch !here.',
        array('!here' => l(t('here'), 'admin/config/services/exact_target_soap/queue'))
      );
      break;
    case 'failure':
      $message['subject'] = t('The ExactTarget API is still in batch mode');
      $message['body'][] = t(
        'The attempt to clear the ExactTarget batch queue failed. There are still items that need to be sent. ' .
          'Please review these items and either re-run the batch job or remove them from the batch queue.'
      );
      break;
  }
}

/**
 * Implements hook_block_info().
 */
function exact_target_soap_block_info() {

  $blocks['list-subscribe'] = array(
    'info' => t('Subscribe to Exact Target lists'),
    'cache' => DRUPAL_NO_CACHE,
  );

  return $blocks;

}

/**
 * Implements hook_block_view().
 */
function exact_target_soap_block_view($delta = '') {

  $block = array();

  switch ($delta) {

    case 'list-subscribe':

      if (user_is_logged_in()) {

        module_load_include('inc', 'exact_target_soap', 'exact_target_soap.subscribe');

        $block['subject'] = t('Subscribe to mailing lists');
        $block['content'] = drupal_get_form('exact_target_soap_subscribe_ui');

      }

      break;

  }

  return $block;

}

/**
 * Processes the queued requests in batch.
 *
 * @param $progressive
 *   Set to TRUE if the batch should be interactive (i.e. for a user interface).
 * @param $timeout
 *   The number of seconds that the batch job should be allowed to execute for.
 */
function exact_target_soap_clear_batch($progressive = FALSE, $timeout = 0) {

  $batch = array(
    'title' => 'Processing ExactTarget SOAP Requests',
    'operations' => array(
      array('exact_target_soap_batch_process', array($timeout)),
    ),
    'finished' => 'exact_target_soap_batch_finished',
    'file' => drupal_get_path('module', 'exact_target_soap') . '/exact_target_soap.batch.inc',
  );

  batch_set($batch);
  $batch =& batch_get();
  $batch['progressive'] = $progressive;
  batch_process();
}

/**
 * Returns TRUE if the ET API is responding, else FALSE.
 *
 * @param $result
 *   Will contain the result object after the status is checked.
 *   This is useful for getting detail on why the status is bad.
 * @param $reset
 *   Flag that will determine if it's safe to disable batch mode.
 * @return
 *   TRUE if the API status is 'OK', else FALSE.
 */
function exact_target_soap_status(stdClass &$result = NULL, $reset = FALSE) {
  $ret = TRUE;
  $message = '';
  $vars = array();
  $request = new ExactTarget();

  try {
    $result = @ExactTarget::instance()->GetSystemStatus($request);
  } catch (Exception $e) {
    $ret = FALSE;
    $message = 'Your site failed to connect to the ExactTarget API.';
  }

  if (!isset($result->Results->Result->SystemStatus)) {
    $ret = FALSE;
    $message = 'Your site failed to connect to the ExactTarget API.';
  }
  elseif ($result->Results->Result->SystemStatus != 'OK') {
    $ret = FALSE;
    $message = 'There was a problem connecting to the ExactTarget API: @status: @message.';
    $vars = array(
      '@status' => $result->Results->Result->SystemStatus,
      '@message' => $result->Results->Result->StatusMessage
    );
  }
  else {
    variable_set('exact_target_soap_batch', FALSE);
  }

  // If there's a problem put the site into batch mode, log it, and notify.
  if ($ret == FALSE) {
    variable_set('exact_target_soap_batch', TRUE);
    watchdog('exact_target_soap', $message, $vars, WATCHDOG_ERROR);

    if (variable_get('exact_target_soap_cleanup', EXACT_TARGET_SOAP_CLEANUP_NOTHING) == EXACT_TARGET_SOAP_CLEANUP_MAIL) {
      $to = variable_get('exact_target_soap_mail', variable_get('site_mail', ''));
      drupal_mail('exact_target_soap', 'cleanup', $to, language_default());
    }

    return $ret;
  }

  // Take the site out of batch mode if the connection was successful.
  if ($reset && variable_get('exact_target_soap_batch', FALSE)) {
    variable_set('exact_target_soap_batch', FALSE);
  }
  return $ret;
}

/**
 * This function checks to see if the uids are safe to attempt to sync
 * ExactTarget even though the site is in "queuing mode" and there is a
 * queue built up. If any of the uids appear in the queue, or if there
 * are requests in the queue with NULL for the UID, then it is not safe
 * to simply attempt to live-sync requests involving those users, and
 * those requests should instead be inserted in the queue.
 *
 * @param $uids
 *   An array of integer user ids.
 * @return
 *   TRUE if those users can be synced using the API, even though there is a
 *   backlog of queued API requests.
 */
function exact_target_soap_request_safe_for_live(array $uids) {
  $sql = 'SELECT count(bid) FROM {exact_target_soap_batch} where uid IS NULL';
  $args = array();

  if (count($uids) != 0) {
    $sql = $sql . ' OR uid IN (' . db_placeholders($uids) . ')';
    $args = array_merge($args, $uids);
  }
  if (db_query($sql, $args)->fetchField()) {
    return FALSE;
  }
  return TRUE;
}

/**
 * Retrieves all lists for this Exact Target account.
 *
 * @return array
 *   Lists with their properties.
 */
function exact_target_soap_get_lists() {

  try {

    // Create the Soap Client
    $request = new ExactTarget();
    $client = $request->client;

    $rr = new ExactTarget_RetrieveRequest();
    $rr->ObjectType = "List";
    $rr->Properties =  array();
    $rr->Properties[] = "ID";
    $rr->Properties[] = "List.ListName";
    $rr->Properties[] = "Description";
    $rr->Properties[] = "ListClassification";
    $rr->Properties[] = "Category";
    $rr->Properties[] = "CreatedDate";
    $rr->Properties[] = "ModifiedDate";
    $rr->Properties[] = "Type";
    $rr->Options = NULL;

    // There is a default Exact Target "All Subscribers" list that is used for
    // bulk subscribing/unsubscribing a user from all lists. This is a master
    // list and probably shouldn't be presented to the user because it behaves
    // differently than ordinary lists. It has a "master" unsubscribe
    // characteristic that when unsubscribed from causes any previously
    // subscribed to lists to have to be reactivated manually. When a list goes
    // into that state, the API will no longer function "normally", that is as
    // expected. Due to this, it's best to pre-emptively remove this special
    // list to avoid problems.
    // @see  http://help.exacttarget.com/en-US/documentation/exacttarget/system_guides/unsubscribes/#section_24
    $sfp = new ExactTarget_SimpleFilterPart();
    $sfp->Property = "ListName";
    $sfp->SimpleOperator = ExactTarget_SimpleOperators::notEquals;
    $sfp->Value = "All Subscribers";

    $rr->Filter = new SoapVar($sfp, SOAP_ENC_OBJECT, 'SimpleFilterPart', "http://exacttarget.com/wsdl/partnerAPI");

    $rrm = new ExactTarget_RetrieveRequestMsg();
    $rrm->RetrieveRequest = $rr;

    $results = $client->Retrieve($rrm);

  }
  catch (Exception $e) {

    $results = FALSE;

  }

  if (!isset($results->Results)) {

    return FALSE;

  }

  return $results->Results;

}

/**
 * Retrieves the master Exact Target List.
 *
 * @return array
 *   Master List with its properties.
 */
function exact_target_soap_get_master_list() {

  try {

    // Create the Soap Client
    $request = new ExactTarget();
    $client = $request->client;

    $rr = new ExactTarget_RetrieveRequest();
    $rr->ObjectType = "List";
    $rr->Properties =  array();
    $rr->Properties[] = "ID";
    $rr->Properties[] = "List.ListName";
    $rr->Properties[] = "Description";
    $rr->Properties[] = "ListClassification";
    $rr->Properties[] = "Category";
    $rr->Properties[] = "CreatedDate";
    $rr->Properties[] = "ModifiedDate";
    $rr->Properties[] = "Type";
    $rr->Options = NULL;

    // There is a default Exact Target "All Subscribers" list that is used for
    // bulk subscribing/unsubscribing a user from all lists. This is a master
    // list and probably shouldn't be presented to the user because it behaves
    // differently than ordinary lists. It has a "master" unsubscribe
    // characteristic that when unsubscribed from causes any previously
    // subscribed to lists to have to be reactivated manually. When a list goes
    // into that state, the API will no longer function "normally", that is as
    // expected. Due to this, it's best to pre-emptively remove this special
    // list to avoid problems.
    // @see  http://help.exacttarget.com/en-US/documentation/exacttarget/system_guides/unsubscribes/#section_24
    $sfp = new ExactTarget_SimpleFilterPart();
    $sfp->Property = "ListName";
    $sfp->SimpleOperator = ExactTarget_SimpleOperators::equals;
    $sfp->Value = "All Subscribers";

    $rr->Filter = new SoapVar($sfp, SOAP_ENC_OBJECT, 'SimpleFilterPart', "http://exacttarget.com/wsdl/partnerAPI");

    $rrm = new ExactTarget_RetrieveRequestMsg();
    $rrm->RetrieveRequest = $rr;

    $results = $client->Retrieve($rrm);

  }
  catch (Exception $e) {

    $results = FALSE;

  }

  if (!isset($results->Results)) {

    return FALSE;

  }

  return $results->Results;

}

/**
 * Retreives a subscriber by email.
 *
 * @param string $email
 *   The email address of the subscriber.
 *
 * @return array
 *   Returns a subscriber information from Exact Target.
 */
function exact_target_soap_get_subscriber_by_email($email) {
  $subscriber = new stdClass();

  try {
    $rr = new ExactTarget_RetrieveRequest();
    $rr->ObjectType = "Subscriber";
    $rr->Properties = array();
    $rr->Properties[] = "ID";
    $rr->Properties[] = "SubscriberKey";
    $rr->Properties[] = "EmailAddress";
    $rr->Properties[] = "Status";

    $sfp = new ExactTarget_SimpleFilterPart();
    $sfp->Value = array($email);
    $sfp->SimpleOperator = ExactTarget_SimpleOperators::equals;
    $sfp->Property = "EmailAddress";

    $rr->Filter = new SoapVar($sfp, SOAP_ENC_OBJECT, 'SimpleFilterPart', "http://exacttarget.com/wsdl/partnerAPI");
    $rr->Options = NULL;
    $rrm = new ExactTarget_RetrieveRequestMsg();
    $rrm->RetrieveRequest = $rr;

    $rrm = new ExactTarget_RetrieveRequestMsg();
    $rrm->RetrieveRequest = $rr;

    $results = @ExactTarget::instance()->Retrieve($rrm);
  }
  catch (Exception $e) {
    $results = FALSE;
  }

  if (isset($results->Results)) {
    $subscriber = $results->Results;
  }

  return $subscriber;
}


/**
 * Retreives a subscriber list by email.
 *
 * @param string $email
 *   The email address of the subscriber.
 * @param string $business_unit
 *   (Optional) The business unit account ID of the subaccount.
 *
 * @return array
 *   Returns a subscriber's list information from Exact Target.
 */
function exact_target_soap_get_subscriber_lists_by_email($email, $business_unit = NULL) {
  $subscriber_lists = array();

  try {
    $rr = new ExactTarget_RetrieveRequest();
    $rr->ObjectType = 'ListSubscriber';
    $rr->QueryAllAccounts = TRUE;

    // Set the properties to return.
    $props = array("ListID", "SubscriberKey", "Status");
    $rr->Properties = $props;

    // Setup account filtering, to look for a given account MID.
    $filter_part = new ExactTarget_SimpleFilterPart();
    $filter_part->Property = 'SubscriberKey';
    $values = array($email);
    $filter_part->Value = $values;
    $filter_part->SimpleOperator = ExactTarget_SimpleOperators::equals;

    // Encode the SOAP package.
    $filter_part = new SoapVar($filter_part, SOAP_ENC_OBJECT,'SimpleFilterPart', "http://exacttarget.com/wsdl/partnerAPI");

    // Add business unit if passed in.
    if ($business_unit) {
      $client_filter_part = new ExactTarget_SimpleFilterPart();
      $client_filter_part->Property = 'SubscriberKey';
      $client_id = array($email);
      $client_filter_part->Value = $values;

      // The account ID of the subaccount.
      $client->ID = $business_unit;
    }

    // Set the filter to NULL to return all MIDs, otherwise set to filter object.
    $rr->Filter = $filter_part;

    // Setup and execute request.
    $rrm = new ExactTarget_RetrieveRequestMsg();
    $rrm->RetrieveRequest = $rr;
    $results = @ExactTarget::instance()->Retrieve($rrm);
  }
  catch (Exception $e) {
    $results = FALSE;
  }

  if (isset($results->Results)) {
    $subscriber_lists = $results->Results;
  }

  return $subscriber_lists;
}


/**
 * Retreives a subscriber by ListID.
 *
 * @param string $list_id
 *   The ID of the list to get information from.
 *
 * @return array
 *   Returns subscribers information for the list from Exact Target.
 */
function exact_target_soap_get_subscribers_by_list_id($list_id) {
  $subscribers = new stdClass();

  try {
    $rr = new ExactTarget_RetrieveRequest();
    $rr->ObjectType = "SubscriberList";
    $rr->Properties = array();
    $rr->Properties[] = "ID";
    $rr->Properties[] = "List.ListName";
    $rr->Properties[] = "List.ID";
    $rr->Properties[] = "Status";
    $rr->Properties[] = "Subscriber.EmailAddress";

    $sfp = new ExactTarget_SimpleFilterPart();

    // This is the ListID that you want to get the subscribers for.
    $sfp->Value = array($list_id);
    $sfp->SimpleOperator = ExactTarget_SimpleOperators::equals;
    $sfp->Property = "List.ID";

    $rr->Filter = new SoapVar($sfp, SOAP_ENC_OBJECT, 'SimpleFilterPart', "http://exacttarget.com/wsdl/partnerAPI");
    $rr->Options = NULL;

    // Setup and execute request.
    $rrm = new ExactTarget_RetrieveRequestMsg();
    $rrm->RetrieveRequest = $rr;
    $results = @ExactTarget::instance()->Retrieve($rrm);
    dsm($results);
  }
  catch (Exception $e) {
    $results = FALSE;
  }

  if (isset($results->Results)) {
    //$subscriber = $results->Results;
  }

  return $subscriber;
}


/**
 * Subscribe a user to a mailing list.
 *
 * @param array $account_info
 *   Account information that must include email, but could also include
 *   first_name and last_name.
 * @param string $list_id
 *   The ID of the list to subscribe the user to.
 * @param string $business_unit
 *   (Optional) The business unit account ID of the subaccount.
 *
 * @return array
 *   Returns a subscriber's information from Exact Target.
 */
function exact_target_soap_upsert_subscribe_to_list_id($account_info, $list_id, $business_unit = NULL) {
  // Upsert Subscriber.
  _exact_target_soap_upsert_subscriber($account_info);

  // Subscribe to Mailing Lists.
  return _exact_target_soap_upsert_subscribers_to_list_subscribe($account_info, $list_id, $business_unit, TRUE);
}

/**
 * Subscribe a user to a mailing list.
 *
 * @param array $account_info
 *   Account information that must include email, but could also include
 *   first_name and last_name.
 * @param string $list_id
 *   The ID of the list to subscribe the user to.
 * @param string $business_unit
 *   (Optional) The business unit account ID of the subaccount.
 *
 * @return array
 *   Returns a subscriber's information from Exact Target.
 */
function exact_target_soap_upsert_unsubscribe_to_list_id($account_info, $list_id, $business_unit = NULL) {
  // Upsert Subscriber.
  _exact_target_soap_upsert_subscriber($account_info);

  // Unsubscribe from Mailing Lists.
  return _exact_target_soap_upsert_subscribers_to_list_unsubscribe($account_info, $list_id, $business_unit, FALSE);
}

/**
 * Creates a triggered send request.
 *
 * Sends an email to a subscriber in response to an event. For example, when a
 * user is subscribed to a new lists can send a confirmation email to provide
 * feedback to the user that the subscription was completed.
 *
 * SPECIAL NOTE
 *
 *   The $list_id parameter passed in must match the "External Key" property of
 *   the Triggered Send item that is to be sent. This means that a Triggered
 *   Send item must be setup in the Exact Target UI that has an "External Key"
 *   property value matching what gets sent to this function. If there is no
 *   item matching, the Triggered Send will fail and error out.
 *
 * @param array $account_info
 *   Account information that must include email, but could also include
 *   first_name and last_name.
 * @param string $list_id
 *   The ID of the list to subscribe the user to.
 * @param string $business_unit
 *   (Optional) The business unit account ID of the subaccount.
 *
 * @return object
 *   Response object from the ExactTarget API.
 */
function exact_target_soap_triggered_send($account_info = array(), $list_id = NULL, $business_unit = NULL) {
  $results = array();

  try {

    // Create
    $et_triggered_send_def = new ExactTarget_TriggeredSendDefinition();

    // The "CustomerKey" property of this object is synonymous with the
    // "External Key" property of a Triggered Send email as configured via the
    // Exact Target administrative UI. What this means is that for a Triggered
    // Send email to actually be sent, the Exact Target administrator must
    // manually create a corresponding Triggered Send email with the "List ID"
    // of the list set to the "External Key" property of the Triggered Send. If
    // there is no such Triggered Send setup, this request will fail with an
    // error stating "Invalid Customer Key".
    $et_triggered_send_def->CustomerKey = $list_id;

    $email = '';
    if (isset($account_info['email']) && !empty($account_info['email'])) {
      $email = $account_info['email'];
    }
    elseif (isset($account_info['Email']) && !empty($account_info['Email'])) {
      $email = $account_info['Email'];
    }

    if (empty($email)) {

      throw new Exception('Email account property is empty.');

    }

    $subscriber = new ExactTarget_Subscriber();
    $subscriber->EmailAddress = $email;
    $subscriber->SubscriberKey = $email;
    $subscriber->Lists = array();

    // What value is there in providing these properties on the subscriber
    // object here? Is this redundant? If not, should all additional properties
    // be added (ie. not just first and last name)?
    $first = '';
    if (isset($account_info['first_name']) && !empty($account_info['first_name'])) {
      $first = $account_info['first_name'];
    }
    if (isset($account_info['First Name']) && !empty($account_info['First Name'])) {
      $first = $account_info['First Name'];
    }

    $last = '';
    if (isset($account_info['last_name']) && !empty($account_info['last_name'])) {
      $last = $account_info['last_name'];
    }
    if (isset($account_info['Last Name']) && !empty($account_info['Last Name'])) {
      $last = $account_info['Last Name'];
    }

    if (!empty($first_name)) {

      $first_name = new ExactTarget_Attribute();
      $first_name->Name = "First Name";
      $first_name->Value = $first;

      $subscriber->Attributes = array($first_name);

    }

    if (!empty($last)) {

      $last_name = new ExactTarget_Attribute();
      $last_name->Name = "Last Name";
      $last_name->Value = $last;

      $subscriber->Attributes = array($last_name);

    }

    $triggered_send = new ExactTarget_TriggeredSend();
    $triggered_send->Subscribers = array($subscriber);
    $triggered_send->TriggeredSendDefinition = $et_triggered_send_def;

    if ($business_unit) {
      $cl = new ExactTarget_ClientID();
      $cl->ID = $business_unit;
      $triggered_send->Client = $cl;
    }

    $object = new SoapVar($triggered_send, SOAP_ENC_OBJECT, 'TriggeredSend', "http://exacttarget.com/wsdl/partnerAPI");

    $request = new ExactTarget_CreateRequest();
    $request->Objects = array($object);

    $results = @ExactTarget::instance()->Create($request);
  }
  catch (Exception $e) {
    $results = FALSE;
  }

  return $results;

}


/**
 * Upsert a Subscriber to ExactTarget. Does not add the subscriber to a list or update their status to any list.
 *
 * @param array $account_info
 *   Account information that must include email, but could also include
 *   first_name and last_name.
 *
 * @return array
 *   Returns a subscriber's information from Exact Target.
 */
function _exact_target_soap_upsert_subscriber($account_info) {
  $results = array();

  try {
    $subscriber = new ExactTarget_Subscriber();
    $subscriber->EmailAddress = $account_info['email'];
    $subscriber->SubscriberKey = $account_info['email'];
    $subscriber->Lists = array();

    if (isset($account_info['first_name'])) {

      $first_name = new ExactTarget_Attribute();
      $first_name->Name = "First Name";
      $first_name->Value = $account_info['first_name'];

      $subscriber->Attributes = array($first_name);

    }

    if (isset($account_info['last_name'])) {

      $last_name = new ExactTarget_Attribute();
      $last_name->Name = "Last Name";
      $last_name->Value = $account_info['last_name'];

      $subscriber->Attributes = array($last_name);

    }

    // This is the section needed to define it as an "Upsert"
    $so = new ExactTarget_SaveOption();
    $so->PropertyName = "*";
    $so->SaveAction = ExactTarget_SaveAction::UpdateAdd;
    $soe = new SoapVar($so, SOAP_ENC_OBJECT, 'SaveOption', "http://exacttarget.com/wsdl/partnerAPI");
    $opts = new ExactTarget_UpdateOptions();
    $opts->SaveOptions = array($soe);

    // Set Subscriber status to Active.
    $subscriber->Status = ExactTarget_SubscriberStatus::Active;

    $object = new SoapVar($subscriber, SOAP_ENC_OBJECT, 'Subscriber', "http://exacttarget.com/wsdl/partnerAPI");

    $request = new ExactTarget_CreateRequest();
    $request->Options = $opts;
    $request->Objects = array($object);

    $results = @ExactTarget::instance()->Create($request);
  }
  catch (Exception $e) {
    $results = FALSE;
  }

  return $results;
}

/**
 * Subscribes a Subscriber to a given list.
 * Based on Example from: https://help.exacttarget.com/en/technical_library/web_service_guide/technical_articles/managing_subscribers_on_lists/
 *
 * @param array $account_info
 *   Account information that must include email, but could also include
 *   first_name and last_name.
 * @param string $list_id
 *   The ID of the list to subscribe the user to.
 * @param string $business_unit
 *   (Optional) The business unit account ID of the subaccount.
 * @return array
 *   Returns a subscriber's information from Exact Target.
 */
function _exact_target_soap_upsert_subscribers_to_list_subscribe($account_info, $list_id, $business_unit = NULL) {
  $results = array();

  try {
    $subscriber = new ExactTarget_Subscriber();
    $subscriber->EmailAddress = $account_info['email'];
    $subscriber->SubscriberKey = $account_info['email'];

    $subscriber->Lists = array();
    $list = new ExactTarget_SubscriberList();
    // This is the ID of the subscriber list.
    $list->ID = $list_id;
    $list->Status = ExactTarget_SubscriberStatus::Active;

    if ($business_unit) {
      $cl = new ExactTarget_ClientID();
      $cl->ID = $business_unit;
      $subscriber->Client = $cl;

    }

    $subscriber->Lists[] = $list;

    // This is the section needed to define it as an "Upsert"
    $so = new ExactTarget_SaveOption();
    $so->PropertyName = "*";
    $so->SaveAction = ExactTarget_SaveAction::UpdateAdd;
    $soe = new SoapVar($so, SOAP_ENC_OBJECT, 'SaveOption', "http://exacttarget.com/wsdl/partnerAPI");
    $opts = new ExactTarget_UpdateOptions();
    $opts->SaveOptions = array($soe);

    // Set subscription status.
    $subscriber->Status = ExactTarget_SubscriberStatus::Active;

    $object = new SoapVar($subscriber, SOAP_ENC_OBJECT, 'Subscriber', "http://exacttarget.com/wsdl/partnerAPI");

    $request = new ExactTarget_CreateRequest();
    $request->Options = $opts;
    $request->Objects = array($object);

    $results = @ExactTarget::instance()->Create($request);
  }
  catch (Exception $e) {
    $results = FALSE;
  }

  return $results;
}


/**
 * Unsubscribe a user from a mailing list.
 *
 * @param array $account_info
 *   Account information that must include email, but could also include
 *   first_name and last_name.
 * @param string $list_id
 *   The ID of the list to subscribe the user to.
 * @param string $business_unit
 *   (Optional) The business unit account ID of the subaccount.
 *
 * @return array
 *   Returns a subscriber's information from Exact Target.
 */
function _exact_target_soap_upsert_subscribers_to_list_unsubscribe($account_info, $list_id, $business_unit = NULL) {
  $results = array();

  try {
    $subscriber = new ExactTarget_Subscriber();
    $subscriber->EmailAddress = $account_info['email'];
    $subscriber->SubscriberKey = $account_info['email'];
    $subscriber->Lists = array();

    $list = new ExactTarget_SubscriberList();
    // This is the ID of the subscriber list.
    $list->ID = $list_id;
    $list->Status = ExactTarget_SubscriberStatus::Unsubscribed;

    if ($business_unit) {
      $cl = new ExactTarget_ClientID();
      $cl->ID = $business_unit;
      $subscriber->Client = $cl;

    }

    $subscriber->Lists[] = $list;

    // This is the section needed to define it as an "Upsert"
    $so = new ExactTarget_SaveOption();
    $so->PropertyName = "*";
    $so->SaveAction = ExactTarget_SaveAction::UpdateAdd;
    $soe = new SoapVar($so, SOAP_ENC_OBJECT, 'SaveOption', "http://exacttarget.com/wsdl/partnerAPI");
    $opts = new ExactTarget_UpdateOptions();
    $opts->SaveOptions = array($soe);

    // Set subscription status.
    $subscriber->Status = ExactTarget_SubscriberStatus::Active;

    $object = new SoapVar($subscriber, SOAP_ENC_OBJECT, 'Subscriber', "http://exacttarget.com/wsdl/partnerAPI");

    $request = new ExactTarget_CreateRequest();
    $request->Options = $opts;
    $request->Objects = array($object);

    $results = @ExactTarget::instance()->Create($request);
  }
  catch (Exception $e) {
    $results = FALSE;
  }

  return $results;
}


/**
 * Subscribe a user to a mailing list.
 *
 * SPECIAL NOTE
 *
 *   This function may not behave as expected, specifically related to the
 *   $subscribe_action == FALSE condition. Rather than placing the
 *   ExactTarget_SubscriberStatus on the "List" object, this function places it
 *   on the "Subscriber" object. This means that when attempting to unsubscribe
 *   a user from a particular list, it instead unsubscribes the user from all
 *   lists and places the account in a 'Master' unsubscribed state. The effect
 *   of this state is that the user cannot be reactivated to a previously
 *   subscribed to list without manually unlocking the state in the Exact Target
 *   administrative UI. Exact Target uses this state when a user chooses to
 *   unsubscribe from all mailings for all lists (for instance, by selecting the
 *   "Unsubscribe" link in an email message and proceeding to select a global
 *   unsubscribe option). The 'Master' state probably helps to prevent the user
 *   from being incidentally reactivated to a list at a later point.
 *   Nevertheless, this behavior may not be expected, and as a result it may be
 *   better to use another function.
 *
 *   @see  _exact_target_soap_upsert_subscriptions()
 *
 *
 * @param array $account_info
 *   Account information that must include email, but could also include
 *   first_name and last_name.
 * @param string $list_id
 *   The ID of the list to subscribe the user to.
 * @param string $business_unit
 *   (Optional) The business unit account ID of the subaccount.
 * @param bool $subscribe_action
 *   TRUE - Subscribe the user to the mailing list.
 *   FALSE - Unsubscribe the user to the mailing list.
 *
 * @return array
 *   Returns a subscriber's information from Exact Target.
 */
function _exact_target_soap_upsert_subscribers_to_list($account_info, $list_id, $business_unit = NULL, $subscribe_action = TRUE) {
  $results = array();

  try {
    $subscriber = new ExactTarget_Subscriber();
    $subscriber->EmailAddress = $account_info['email'];
    $subscriber->SubscriberKey = $account_info['email'];
    $subscriber->Lists = array();

    if (isset($account_info['first_name'])) {

      $first_name = new ExactTarget_Attribute();
      $first_name->Name = "First Name";
      $first_name->Value = $account_info['first_name'];

      $subscriber->Attributes = array($first_name);

    }

    if (isset($account_info['last_name'])) {

      $last_name = new ExactTarget_Attribute();
      $last_name->Name = "Last Name";
      $last_name->Value = $account_info['last_name'];

      $subscriber->Attributes = array($last_name);

    }

    $list = new ExactTarget_SubscriberList();
    // This is the ID of the subscriber list.
    $list->ID = $list_id;

    if ($business_unit) {
      $cl = new ExactTarget_ClientID();
      $cl->ID = $business_unit;
      $subscriber->Client = $cl;

    }

    $subscriber->Lists[] = $list;

    // This is the section needed to define it as an "Upsert"
    $so = new ExactTarget_SaveOption();
    $so->PropertyName = "*";
    $so->SaveAction = ExactTarget_SaveAction::UpdateAdd;
    $soe = new SoapVar($so, SOAP_ENC_OBJECT, 'SaveOption', "http://exacttarget.com/wsdl/partnerAPI");
    $opts = new ExactTarget_UpdateOptions();
    $opts->SaveOptions = array($soe);

    // Set subscription status.
    $subscriber->Status = ExactTarget_SubscriberStatus::Active;

    $object = new SoapVar($subscriber, SOAP_ENC_OBJECT, 'Subscriber', "http://exacttarget.com/wsdl/partnerAPI");

    $request = new ExactTarget_CreateRequest();
    $request->Options = $opts;
    $request->Objects = array($object);

    $results = @ExactTarget::instance()->Create($request);
  }
  catch (Exception $e) {
    $results = FALSE;
  }

  return $results;
}

/**
 * Subscribes and unsubscribes user from one or more lists.
 *
 * Manages user's subscription information by creating or reactivating
 * subscriptions to lists or unsubscribing user from lists. One typical use case
 * might be to take user input from a form containing checkboxes for each list,
 * parse the user input and pass the collection of lists that need to be
 * subscribed to or unsubscribed from to this function, which will place the
 * request to the Exact Target API.
 *
 * @param array $account_info
 *   Associative array of account information containing at minimum the email
 *   address of the user (key=Email). Optionally also contains other attributes
 *   that match expected Subscriber object properties in ExactTarget account.
 * @param array $lists
 *   Lists to be updated. Contains object for each list as retrieved from Exact
 *   Target with the exception of additional properties "Subscribe" and
 *   "Status". "Subscribe" is a mandatory boolean determining whether the list
 *   should be subscribed to (TRUE) or unsubscribed from (FALSE). "Status" is an
 *   optional string that carries the list's current status for the user as
 *   defined by ExactTarget_SubscriberStatus (Options are 'Active', 'Bounced',
 *   'Held', 'Unsubscribed', 'Deleted').
 *
 * @return object
 *   Results object containing response status for API request and request
 *   information. This can be inspected for handling the response based on the
 *   response status. Look at $results->Results->StatusMessage and
 *   $results->Results->StatusMessage.
 */
function _exact_target_soap_upsert_subscriptions($account_info, $lists) {

  try {

    if (empty($account_info['Email'])) {

      throw new Exception('Email account property is empty.');

    }

    $subscriber = new ExactTarget_Subscriber();
    $subscriber->EmailAddress = $account_info['Email'];
    $subscriber->SubscriberKey = $account_info['Email'];
    $subscriber->Status =  ExactTarget_SubscriberStatus::Active;

    foreach ($account_info as $name => $value) {

      if ($name == 'Email' || $name == 'PartnerKey') {
        continue;
      }

      $attribute = new ExactTarget_Attribute();
      $attribute->Name = $name;
      $attribute->Value = $value;

      $subscriber->Attributes[] = $attribute;

    }

    if (isset($account_info['PartnerKey'])) {

      $subscriber->PartnerKey = $account_info['PartnerKey'];

    }

    foreach ($lists as $list_id => $list) {

      $subList = new ExactTarget_SubscriberList();
      $subList->ID = $list_id;
      $subList->PartnerKey = $list->PartnerKey;

      if ($list->Subscribe) {

        if (isset($list->Status)) {

          // Action should be set on the SubscriberList to 'update' because this
          // subscriber was previously subscribed to this list at some point.
          $subList->Action = "update";

        }
        else {

          // Action should be set on the SubscriberList to 'create' meaning they
          // will be added if they don't exist. If they already exist, it will
          // return an error.
          $subList->Action = "create";

        }

        $subList->Status = ExactTarget_SubscriberStatus::Active;

      }
      else {

        // Action should be set on the SubscriberList to 'update'. An error will
        // be returned if the subscriber does not already exist on list.
        $subList->Status = ExactTarget_SubscriberStatUnsubscribcribed;
        $subList->Action = "update";

      }

      $subscriber->Lists[] = $subList;

    }

    $object = new SoapVar($subscriber, SOAP_ENC_OBJECT, 'Subscriber', "http://exacttarget.com/wsdl/partnerAPI");

    // This is the section needed to define it as an "Upsert"
    $so = new ExactTarget_SaveOption();
    $so->PropertyName = "*";
    $so->SaveAction = ExactTarget_SaveAction::UpdateAdd;
    $soe = new SoapVar($so, SOAP_ENC_OBJECT, 'SaveOption', "http://exacttarget.com/wsdl/partnerAPI");
    $opts = new ExactTarget_UpdateOptions();
    $opts->SaveOptions = array($soe);

    $request = new ExactTarget_CreateRequest();
    $request->Options = $opts;
    $request->Objects = array($object);
    $results = @ExactTarget::instance()->Update($request);

  }
  catch (Exception $e) {

    $results = FALSE;
    watchdog('exact_target_soap', 'Exact Target API request failed: !replace', array('!replace' => '<pre>' . print_r($e, TRUE) . '</pre>'), WATCHDOG_ERROR);

  }

  return $results;

}

/**
 * Utility comparison function for use as callback for usort(), uasort(), etc.
 */
function _exact_target_soap_order_list_alpha($a, $b) {

  if ($a == $b) {

    return 0;

  }

  return (strtolower($a->ListName) < strtolower($b->ListName)) ? -1 : 1;

}

/**
 * Reactivates a subscriber by setting the user to Active on the Master List.
 */
function exact_target_soap_activate_master_activate($email) {
  // Fetch the master list.
  $master_list = exact_target_soap_get_master_list();

  if ($master_list && $master_list->ID) {
    // Subscribe the user to the master list.
    $lists = array();
    $master_list->Subscribe = TRUE;
    $master_list->Status = TRUE;
    $lists[$master_list->ID] = $master_list;
    $account_info['Email']  = $email;
    _exact_target_soap_upsert_subscriptions($account_info, $lists);
  }
}
