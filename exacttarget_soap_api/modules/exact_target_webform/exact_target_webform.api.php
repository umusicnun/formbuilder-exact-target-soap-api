<?php

/**
 * @file
 * Hooks provided by ExactTarget Webform module.
 */

/**
 * @defgroup exact_target_webform ExactTarget Webform Module Hooks
 * @{
 * ExactTarget Webform's hooks enable other modules to alter data, which changes
 * the options available to administrators when configuring a webform.
 */

/**
 * Alter the ExactTarget properties that can be mapped to webform fields.
 *
 * Modules may want to provide their own hardcoded properties that match their
 * ExactTarget implementation. Better yet, a module may want to implement a
 * method that queries ExactTarget API to dynamically retrieve properties
 * available in their account and use those instead of the hardcoded ones.
 * Unfortunately, as of the writing of this module, the only way to do this is
 * to request a single Subscriber object from the ExactTarget API and inspect
 * its properties to get a list of the available properties. This is unfortunate
 * because it requires having a subscriber's email address already, or requires
 * that a subscriber be created, retrieved, and then deleted. As a result, if
 * such a method is implemented, it's likely that the properties that are
 * retrieved should be cached.
 *
 * Take note that at minimum the "Email" property must exist.
 *
 * @param array $exact_target_properties
 *   Associative array of ExactTarget Subscriber object properties that are
 *   presented to the administrator as options for mapping Webform fields to
 *   ExactTarget properties. These properties will then be saved to the user's
 *   Subscriber object in ExactTarget when the webform is filled out.
 */
function hook_exact_target_webform_properties_alter(&$exact_target_properties) {

  unset($exact_target_properties['Middle Name']);

  $exact_target_properties['Cell Phone'] = t('Cell Phone');

}
