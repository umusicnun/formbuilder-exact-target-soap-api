# ExactTarget Webform

This sub-module provides integration with Webform module and the ExactTarget API, utilizing the exact_target_soap API module. It hooks into Webform to provide its own configuration page that allows administrators to setup webforms that will enable end users to subscribe to ExactTarget lists via the webform. The primary mechanism for this is a mapping of webform fields to ExactTarget API properties.

## Installation and setup instructions

1. Enable the module.
2. Create a webform or navigate to an existing webform.
3. Select the ExactTarget tab, or if not visible, navigate to /node/[nid]/webform/exact-target
4. Fill out the form ensuring to turn it on and map at least a webform email field to the ExactTarget email property.
5. Navigate to the end user facing webform and make a submission.
6. Ensure that the submission subscribed the user to the appropriate ExactTarget lists.


### Optional

This module utilizes the Select or Other (select_or_other) module if it is available to allow "other" input in the select field type so that administrators can enter custom properties for mapping purposes to the ExactTarget Subscriber object properties. If select_or_other is not installed,
it will fall back to a simple select field.
