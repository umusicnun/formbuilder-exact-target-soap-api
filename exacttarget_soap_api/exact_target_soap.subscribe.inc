<?php

/**
 * @file
 * Subscribe functionality for the exact_target_soap module.
 */

/**
 * TODOS
 *
 * - Figure out where JanRain is getting the business ID from in the ET API. For
 *   instance, what is the ET property that corresponds with "Business ID"?
 *   Business ID seems to correlate to Partner Key, which is used in ET API for
 *   assigning ownership to objects, in particular when dealing with ET accounts
 *   that have relationships to each other. When the Partner Key is present, it
 *   ensures that the operations related to the object happen in the appropriate
 *   ET account. However, it remains unclear to what extend support for this
 *   needs to be extended here. For instance, below it was added to the
 *   account_info if present, and passed along to the API request functions.
 *   But, it's not clear if this method is correct.
 *   @see _exact_target_soap_build_subscriber_info()
 *   @see _exact_target_soap_upsert_subscribers_to_list()
 * - Consider converting this to a class so that it can be extended to enable
 *   modules to be able to swap out things like user credentials coming from
 *   another source.
 * - Remove Janrain bridge functions for the javascript embed and place in
 *   separate submodule.
 */


/**
 * AJAX form for subscribing/unsubscribing to Exact Target lists.
 *
 * @see exact_target_soap_subscribe_submit()
 *
 * @ingroup forms
 */
function exact_target_soap_subscribe_ui($form, &$form_state) {

  global $user;

  // This is required for the ajax system. Caches this include path so
  // system/ajax knows where to look for the form handlers.
  form_load_include($form_state, 'inc', 'exact_target_soap', 'exact_target_soap.subscribe');

  $form = array();

  $form['#prefix'] = '<div id="subscribe">';
  $form['#suffix'] = '</div>';

  $form['description'] = array(
    '#title' => t('Description'),
    '#markup' => '<p>' . t('Select the mailing lists that you\'d like to subscribe to.') . '</p>',
  );

  $form_state['account_info'] = _exact_target_soap_build_subscriber_info($user);

  _exact_target_soap_subscribe_ui_build_checkboxes($form, $form_state);

  if (isset($form['error-no-lists'])) {

    watchdog('exact_target_soap', 'The Exact Target site opt-in list setting is empty and the form for users to select lists to subscribe to cannot be rendered. Update the site opt-in list in the <a href="@settings">Exact Target settings</a>.', array('@settings' => url('admin/config/services/exact_target_soap/opt-in')), WATCHDOG_ERROR);

    return $form;

  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#ajax' => array(
      'callback' => 'exact_target_soap_subscribe_ui_callback',
      'wrapper' => 'subscribe',
    ),
    '#value' => t('Subscribe'),
  );

  $form_state['previously_submitted'] = 0;

  return $form;

}

/**
 * Callback for ajax submit button.
 *
 * @return array
 *   Renderable array (the box element)
 *
 * @see exact_target_soap_subscribe()
 *
 * @ingroup forms
 */
function exact_target_soap_subscribe_ui_callback($form, $form_state) {

  return $form;

}

/**
 * Submit handler for AJAX form for subscribing/unsubscribing to Exact Target.
 *
 * @see exact_target_soap_subscribe()
 *
 * @ingroup forms
 */
function exact_target_soap_subscribe_ui_submit($form, &$form_state) {

  $response = _exact_target_soap_subscribe_ui_process_user_input($form_state);

  _exact_target_soap_process_api_response($response, $form_state['updated_subscriptions']);

}

/**
 * Form for embedding Exact Target subscription process via javascript.
 *
 * Retrieves same subscription form, but does not include the submit button and
 * other extraneous parts that aren't needed for embedding the checkbox options
 * into another form process.
 *
 * @see exact_target_soap_subscribe_ui()
 * @see exact_target_soap_subscribe_ui_for_embed_submit()
 *
 * @ingroup forms
 */
function exact_target_soap_subscribe_ui_for_embed() {

  $form = array();

  // May want to retrieve information from the GET request to create an account
  // object if it's an anonymous user but we know the email address of the user
  // as taken from a registration form.
  // $account = new stdClass();
  // $account->uid = ?;
  // $account->mail = $_GET['email'];

  $form['description'] = array(
    '#title' => t('Description'),
    '#markup' => '<p>' . t('Select the mailing lists that you\'d like to subscribe to.') . '</p>',
  );

  $form_state['account_info'] = _exact_target_soap_build_subscriber_info();

  _exact_target_soap_subscribe_ui_build_checkboxes($form, $form_state);

  if (isset($form['error-no-lists'])) {

    watchdog('exact_target_soap', 'The Exact Target site opt-in list setting is empty and the form for users to select lists to subscribe to cannot be rendered. Update the site opt-in list in the <a href="@settings">Exact Target settings</a>.', array('@settings' => url('admin/config/services/exact_target_soap/opt-in')), WATCHDOG_ERROR);

    return $form;

  }

  return $form;

}

/**
 * Pseudo-form submit handler for ajax processing of embedded subscription form.
 *
 * Utilizes the same submission processing of subscriptions as is utilized by
 * the parent form but manually passes the results via custom ajax and then
 * reconstructs the $form_state from scratch.
 *
 * @return array
 *   Response message to pass back to the ajax handler for use in javascript
 *   front end.
 *
 * @see exact_target_soap_subscribe_ui()
 * @see exact_target_soap_subscribe_ui_for_embed()
 *
 * @ingroup forms
 */
function exact_target_soap_subscribe_ui_for_embed_submit() {

  $form_state = array();

  $user_info = $_POST['user'];
  unset($_POST['user']);

  if (empty($user_info['email'])) {
    // TODO: Error message.
    return;
  }

  $account = new stdClass();
  $account->mail = $user_info['email'];

  // Set previously_submitted so that it forces fresh subscriptions array to be
  // retrieved.
  $form_state['previously_submitted'] = 1;
  $form_state['subscriptions'] = array();
  $form_state['opt_in_lists'] = variable_get('exact_target_soap_opt_in', array());
  $form_state['account_info'] = _exact_target_soap_build_subscriber_info($account);

  $form_state['values'] = $_POST;

  if (empty($form_state['values'])) {
    return;
  }

  $response = _exact_target_soap_subscribe_ui_process_user_input($form_state);

  // Inspect API response and alter message on fail.
  if ($response->OverallStatus == "OK") {

    $confirmation_message = _exact_target_soap_ui_confirm_message($form_state['updated_subscriptions']);

    return $confirmation_message;

  }
  else {

    if (isset($response->Results)) {

      return t('There was a problem subscribing/unsubscribing: @error_message. Please contact the site.', array('@error_message' => $response->Results->StatusMessage));
      watchdog('exact_target_soap', 'Exact Target API request error: !replace', array('!replace' => '<pre>' . print_r($response, TRUE) . '</pre>'), WATCHDOG_ERROR);

    }

  }

}

/**
 * Builds Exact Target list options into form array.
 *
 * Passes $form and $form_state back by reference. Adds lists and subscriptions
 * to $form_state for use in form processing later.
 *
 * @param array $form
 *   Drupal form.
 * @param array $form_state
 *   Drupal form state including account_info array containing account
 *   information of the current user.
 *   @see _exact_target_soap_build_subscriber_info()
 */
function _exact_target_soap_subscribe_ui_build_checkboxes(&$form, &$form_state) {

  $cid = 'subscriber_list' . '::email::' . $form_state['account_info']['Email'];
  $subscriptions = _exact_target_soap_get_static_subscriptions($cid, $form_state['account_info']['Email']);
  $opt_in_lists = variable_get('exact_target_soap_opt_in', array());

  $form_state['subscriptions'] = $subscriptions[$cid];
  $form_state['opt_in_lists'] = $opt_in_lists;

  if (!empty($opt_in_lists)) {

    // Sort lists alphabetically by ListName.
    uasort($opt_in_lists, '_exact_target_soap_order_list_alpha');

    foreach ($opt_in_lists as $list) {

      $form['lists'][$list->ID] = array(
        '#title' => $list->ListName,
        '#type' => 'checkbox',
        '#default_value' => (isset($subscriptions[$cid][$list->ID]) && $subscriptions[$cid][$list->ID]->Status == 'Active') ? 1 : 0,
        '#attributes' => array('name' => $list->ID),
      );

    }

  }
  else {

    $form['error-no-lists'] = array(
      '#type' => 'markup',
      '#markup' => t('There are no lists to subscribe to. Please contact an administrator.'),
    );

  }

}

/**
 * Process user input from subscription form/s.
 *
 * Decouples processing from submit callback of specific form so it can be
 * reused.
 *
 * @param array $form_state
 *   Form state array from Drupal form.
 *
 * @return object $response
 *   Response data as returned from Exact Target API.
 */
function _exact_target_soap_subscribe_ui_process_user_input(&$form_state) {

  $updated_subscriptions = array();

  // Need to sync up/retrieve new subscription information from ET and inject
  // into $form_state so that if user submits form more than once in same
  // setting, success messages will match expectations.
  if ($form_state['previously_submitted'] > 0) {

    $cid = 'subscriber_list' . '::email::' . $form_state['account_info']['Email'];
    $subscriptions = _exact_target_soap_get_static_subscriptions($cid, $form_state['account_info']['Email']);
    $form_state['subscriptions'] = $subscriptions[$cid];

  }

  ++$form_state['previously_submitted'];

  $subscriptions = $form_state['subscriptions'];
  $opt_in_lists = $form_state['opt_in_lists'];

  foreach ($opt_in_lists as $id => $list) {

    if (isset($form_state['values'][$id])) {

      // Wants to subscribe to a new list.
      if ($form_state['values'][$id] == 1 && (!isset($subscriptions[$id]) || $subscriptions[$id]->Status != 'Active')) {

        $updated_subscriptions[$id] = $opt_in_lists[$id];
        $updated_subscriptions[$id]->Subscribe = TRUE;

        if (isset($subscriptions[$id]->Status)) {

          $updated_subscriptions[$id]->Status = $subscriptions[$id]->Status;

        }

      }

      // Wants to unsubscribe to a previously subscribed list.
      if ($form_state['values'][$id] == 0 && (isset($subscriptions[$id]) && $subscriptions[$id]->Status == 'Active')) {

        $updated_subscriptions[$id] = $opt_in_lists[$id];
        $updated_subscriptions[$id]->Subscribe = FALSE;

      }

    }

  }

  $form_state['updated_subscriptions'] = $updated_subscriptions;

  $response = _exact_target_soap_mailing_list_subscribe($form_state['account_info'], $updated_subscriptions);

  return $response;

}

/**
 * Retrieve previous subscriptions and update new subscription objects.
 *
 * @param array $account_info
 *   Associative array of account information with keys capitalized as expected
 *   by ExactTarget API. The email address is 'Email', the properties are
 *   'First Name', 'Last Name', 'Address1', etc.
 *   @see _exact_target_webform_retrieve_properties()
 * @param array $subscriptions
 *   Subscription objects containing properties from the ExactTarget List
 *   object.
 */
function _exact_target_soap_sync_subscription_data($account_info, &$subscriptions) {

  // Retrieve subscriptions for this email address first.
  $cid = 'subscriber_list' . '::uid::' . $account_info['Email'];
  $previous_subscriptions = _exact_target_soap_get_static_subscriptions($cid, $account_info['Email']);

  foreach ($subscriptions as $id => $list) {

    // If subscription already exists and has a status, add the status to the
    // subscription so that the API requests an update method, rather than a
    // create method.
    if (isset($previous_subscriptions[$cid][$id]) && isset($previous_subscriptions[$cid][$id]->Status)) {

      $subscriptions[$id]->Status = $previous_subscriptions[$cid][$id]->Status;

    }

  }

}

/**
 * Parse the API response and provide appropriate user and admin feedback.
 *
 * @param object $response
 *   Response object returned from ExactTarget API. This is specific to
 *   subscription requests.
 *   @see _exact_target_soap_upsert_subscriptions()
 * @param array $subscriptions
 *   Associative array of updated subscriptions/list objects.
 */
function _exact_target_soap_process_api_response($response, $subscriptions) {

  // Inspect API response and alter message on fail.
  if ($response->OverallStatus == "OK") {

    $confirmation_message = _exact_target_soap_ui_confirm_message($subscriptions);

    drupal_set_message(drupal_render($confirmation_message), 'status');

  }
  else {

    if (isset($response->Results)) {

      drupal_set_message(t('There was a problem subscribing/unsubscribing: @error_message. Please contact the site.', array('@error_message' => $response->Results->StatusMessage)), 'status');
      watchdog('exact_target_soap', 'Exact Target API request error: !replace', array('!replace' => '<pre>' . print_r($response, TRUE) . '</pre>'), WATCHDOG_ERROR);

    }

  }

}

/**
 * Generate the confirmation message for a subscription form submission.
 *
 * @param array $updated_subscriptions
 *   Updated subscriptions containing submission and subscription data.
 *
 * @return array
 *   Confirmation message as render array.
 */
function _exact_target_soap_ui_confirm_message($updated_subscriptions) {

  $subscribe = $unsubscribe = array();
  $message = array();

  if (!empty($updated_subscriptions)) {

    foreach ($updated_subscriptions as $key => $list) {

      if ($list->Subscribe) {

        $subscribe[] = $list->ListName;

      }
      else {

        $unsubscribe[] = $list->ListName;

      }

    }

  }

  if (!empty($subscribe)) {

    $message[] = array(
      '#tag' => 'p',
      '#attributes' => array('style' => 'font-weight:bold;'),
      '#value' => t('Thank you for subscribing!'),
      '#theme' => 'html_tag',
    );

    $message[] = array(
      '#tag' => 'p',
      '#value' => t('You\'re now subscribed to:'),
      '#theme' => 'html_tag',
    );

    $message[] = array(
      '#items' => $subscribe,
      '#theme' => 'item_list',
    );

  }

  if (!empty($unsubscribe)) {

    $message[] = array(
      '#tag' => 'p',
      '#attributes' => array('style' => 'font-weight:bold;'),
      '#value' => t('Thank you for removing your unwanted subscription.'),
      '#theme' => 'html_tag',
    );

    $message[] = array(
      '#tag' => 'p',
      '#value' => t('You\'re now unsubscribed from:'),
      '#theme' => 'html_tag',
    );

    $message[] = array(
      '#items' => $unsubscribe,
      '#theme' => 'item_list',
    );

  }

  return $message;

}

/**
 * Subscribe/unsubscribe the logged in user to the user-selected mailing lists.
 *
 * @param array $account_info
 *   Account information.
 *   @see _exact_target_soap_build_subscriber_info()
 * @param array $lists
 *   Exact Target list/s to act on.
 *
 * @return bool
 *   Subscribe/unsubscribe was successful.
 */
function _exact_target_soap_mailing_list_subscribe($account_info, $lists) {

  $response = _exact_target_soap_upsert_subscriptions($account_info, $lists);

  foreach ($lists as $list_id => $list) {

    // Only send Triggered Send email on subscription, not unsubscribe.
    if ($list->Subscribe) {

      $triggered_send_response = exact_target_soap_triggered_send($account_info, $list_id);

      if ($triggered_send_response->OverallStatus == 'Error') {

        watchdog('exact_target_soap', 'Exact Target API Triggered Send request error. The most likely cause of this is if there is no Triggered Send configured in Exact Target that matches the List ID of this list. For more information on the error message and how to resolve it, visit the <a href="@apidocs">Exact Target API documentation</a>. Response: !replace', array('@apidocs' => 'https://help.exacttarget.com/en-US/technical_library/web_service_guide/triggered_email_scenario_guide_for_developers/', '!replace' => '<pre>' . print_r($triggered_send_response, TRUE) . '</pre>'), WATCHDOG_ERROR);

      }

    }

  }


  return $response;

}

/**
 * Helper to mung $user object into array as expected by receiving functions.
 *
 * @param object $account
 *   User object containing account credentials.
 *
 * @return array
 *   User email, first name, last name, cid, partner key as associative array.
 *
 * @see _exact_target_soap_upsert_subscribers_to_list()
 */
function _exact_target_soap_build_subscriber_info($account = NULL) {

  static $accounts = array();

  $cid = isset($account->mail) ? $account->mail : 'no_user' . mt_rand();

  if (!isset($accounts[$cid])) {

    $accounts[$cid] = array(
      'Email' => '',
      'PartnerKey' => '',
    );

    $accounts[$cid]['Email'] = isset($account->mail) ? $account->mail : '';

    if (!empty($accounts[$cid]['Email'])) {

      $subscriber = exact_target_soap_get_subscriber_by_email($accounts[$cid]['Email']);

      if (!empty($subscriber) && !empty($subscriber->PartnerKey)) {

        $accounts[$cid]['PartnerKey'] = $subscriber->PartnerKey;

      }

    }

  }

  return $accounts[$cid];

}

/**
 * Retrieves staticly cached list of Exact Target list subscriptions for user.
 *
 * @param string $cid
 *   Static cache id.
 * @param string $email
 *   Email address of the user.
 *
 * @return array
 *   Subscriptions retrieved from Exact Target for this user.
 */
function _exact_target_soap_get_static_subscriptions($cid, $email) {

  static $subscriptions = array();

  if (!isset($subscriptions[$cid])) {

    $subscriptions[$cid] = array();

    if (!empty($email)) {

      $list_by_email = exact_target_soap_get_subscriber_lists_by_email($email);

      foreach ($list_by_email as $list) {

        $subscriptions[$cid][$list->ListID] = $list;

      }

    }

  }

  return $subscriptions;

}

/**
 * Delivery callback for hook_menu().
 *
 * Prints just the HTML as passed from the page callback function, without the
 * page header and footer.
 *
 * @param string $page_callback_result
 *   The HTML content as passed through from the page callback function.
 */
function _exact_target_soap_ahah_deliver($page_callback_result) {

  print drupal_render($page_callback_result);

  // Perform end-of-request tasks.
  drupal_page_footer();

}

